# remote-dl

A simple role to set up a "remote downloader"

Performs the following:
- Sets up a openvpn connection (juuust in case)
- Installs tmux and transmission-cli torrent client
- Sets up an ssh tunnel to a gateway bastion host exposing port 22 to it localy allowing to ssh into the remote-dl host even when hidden behind a nat

2 hosts are required, a remote-dl (a raspberry pi or simillar works quite nicely) and a gateway (probably a vps)
Place into 2 separate groups `endpoint` and `gateway`
